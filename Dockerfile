FROM igwn/lalsuite-dev:buster

LABEL name="LALSuite Development - Clang Dev"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# import required key
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# add llvm repository
RUN echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster main" > /etc/apt/sources.list.d/llvm.list

# install clang
RUN apt-get update && apt-get --assume-yes install clang

# clear package cache
RUN rm -rf /var/lib/apt/lists/*
